#include <stdio.h>
int main(){
    int end1,end2,start1,start2,duration, interval[2] = {0,0};
    scanf("%d %d %d %d %d", &duration, &start1, &end1, &start2, &end2);
    if(duration < 1 || duration > 10){
        printf("Impossivel\n");
        return 0;
    }
    if( end1 <= 19 && end2 <= 19 && start1 >= 9 && start2 >= 9){

        if(start1  >= start2){
            interval[0] = start1;
            if(end1 >= end2){
                interval[1] = end2 - duration;
            }else {
                interval[1] = end1 - duration;

            }

        } else if( start2 > start1){
            interval[0] = start2;
            if(end1 >= end2){
                interval[1] = end2 - duration;
            }else {
                interval[1] = end1 - duration;
    
            }
        }
        if(interval[0] > interval[1]){
            printf("Impossivel\n");
            return 0;
        }
        if (interval[0] == interval[1] && interval[0] != 0){
            printf("%d\n", interval[0]);
        } else {
            if( interval[0] != 0 ||  interval[1] !=0){
                printf("%d %d\n", interval[0], interval[1]);
            } else {
                printf("Impossivel\n");
            }
        }

    }else{
        printf("Impossivel\n");

    }
}   