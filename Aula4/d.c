#include <stdio.h>

int main(){
    int h1,m1,h2,m2, tmp;
    scanf("%d %d %d %d", &h1, &m1, &h2, &m2);
    if(h1 >= 0 && h1 <= 23 && h2 >= 0 && h2 <= 23 && m1 >=0 && m1 <=59 &&  m2 >=0 && m2 <=59  ){
        tmp = ((h2 - h1) * 60 + (m2 - m1) >=  0 )? ((h2 - h1) * 60 + (m2 - m1)): (((h2 - h1) * 60 + (m2 - m1))*-1 ) ; 
        printf("Passaram apenas %d minutos!\nQueres dizer, %d horas e %d minutos?!\n", tmp, tmp/60, tmp%60);    
        return 0;
    }
    else {
        return 0;
    }
}