#include <stdio.h>

int main(){
    int number, first,last,multiple = 0, soma = 0;
    scanf("%d\n", &first);
    scanf("%d\n", &number);
    while(number != 0){
        soma += ( number > first ? number : 0  );
        if(number != 0){
            last = number;
        }
        scanf("%d\n", &number);
    }
    if(first % last == 0){
        multiple = 1;
    } 
    if(soma != 0){
        printf("%d\n", soma);
        return 0 ;
    }else if(multiple){
        printf("%d  multiplo de %d\n",first,last);
        return 0; 
    }else {
        printf("%d não multiplo de %d\n",first,last);
        return 0 ;
    }
}