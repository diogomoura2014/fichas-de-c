#include <stdio.h>

int main(){
  int n, x,count = 0, points = 0, number = 0;
  scanf("%d", &n);
  if(n > 2 && n< 30){

    int realValues[n], guessValues[n];
    for(int i=0; i<n; i++){

      scanf("%d", &x);
      if(x >=1 && x <= 9) realValues[i] = x;
    }
    for(int i=0; i<n; i++){
      scanf("%d", &x);
      if(x >=1 && x <= 9) guessValues[i] = x;

    }

    for(int i=0; i<=n; i++){
      if(guessValues[i] == realValues[i]){  
          count++;
          number++;
      }else if(guessValues[i] != realValues[i]){
        if(count > 1){
          points += count*3;
        }else{
          points += count;
        }
        count = 0;
      }
    }
    printf("%d\n%d\n",number,points);
    return 0;
    }
}