#include <stdio.h>

#define ARRMAX 5000
int main(){
  int values[ARRMAX], n, number, i = 0,j = 0,neg = 0, divs = 0;
  scanf("%d", &values[i++]);

  while( values[i-1] != 0){
    scanf("%d", &values[i++]);
  }
  scanf("%d", &n);
  for(int i = 0; i<n; i++){
    scanf("%d", &number);
    while(values[j++] != 0){
      if(number % values[j-1] == 0){
        divs++;
        if(values[j-1] < 0) neg++;
      }
    }
    printf("Resp: Numero de divisores de %d na sequencia = %d\n      Numero dos negativos = %d\n",number, divs, neg);
    neg = 0;
    divs = 0;
    j = 0;

  }  
  return 0;
}