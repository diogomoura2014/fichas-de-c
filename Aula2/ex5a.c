#include <stdio.h>

int main(){
    int triangulo = 1, valor[3], soma = 0;

    for (int i = 0; i < 3 ; i++)
    {
        //printf("valor = ");
        scanf("%d", &valor[i]);

        soma += valor[i];
        if(i == 2){
            for (int i = 0; i < 3 ; i++)
            {

                if(soma - valor[i] > valor[i]){
                    triangulo = 1;
                }
                
            }
        }
        
        if(valor[i]< 0){
            triangulo = 0;
        }
        
            
    }

    
    printf("(%d,%d,%d)  %s", valor[0],valor[1],valor[2],triangulo == 0 ? "não define triangulo" : "define triangulo");
    return 0;
    
}