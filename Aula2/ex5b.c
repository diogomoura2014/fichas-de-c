#include <stdio.h>
#include <string.h>

int main(){
    int triangulo = 1, valor[3], soma = 0;
    char classification[10] = "";
    for (int i = 0; i < 3 ; i++)
    {
        //printf("valor = ");
        scanf("%d", &valor[i]);
        soma += valor[i];
        if(i == 2){
            for (int j = 0; j < 3 ; j++)
            {
                
                if(soma - valor[j] > valor[j]){
                    triangulo = 1;
                }
            } 
        }
        
        if(valor[i]< 0){
            triangulo = 0;
        }
    }   
    if(valor[0] == valor[1] && valor[1]== valor[2]){
        strcat(classification,"Equilatero");

    }else if(valor[0]==valor[1] || valor[1]==valor[2] || valor[0] == valor[2]){
        strcat(classification,"Isósceles");
        
    }else{
        strcat(classification,"Escaleno");
        
    }

    printf("(%d,%d,%d)  %s %s", valor[0],valor[1],valor[2],triangulo == 0 ? "não define triangulo" : "define triangulo",classification);
    return 0;
    
}