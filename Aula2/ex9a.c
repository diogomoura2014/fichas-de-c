#include <stdio.h>

int main(){
    int circunferencia[2], raio,ponto[2];

    //printf("Insira as coordenadas da circunferencia: \n");
    //printf("x: ");
    scanf("%d", &circunferencia[0]);
    //printf("y: ");
    scanf("%d", &circunferencia[1]);
    //printf("insira o raio: ");
    scanf("%d", &raio);
    //printf("Insira as coordenadas do ponto a ser avaliado: ");
    //printf("x: ");
    scanf("%d", &ponto[0]);
    //printf("y: ");
    scanf("%d", &ponto[1]);
    int distancia = (circunferencia[0] - ponto[0])*(circunferencia[0] - ponto[0]) + (circunferencia[1] - ponto[1]) * (circunferencia[1] - ponto[1]);
    if( raio*raio > distancia ){
        printf("(%d,%d): no interior", ponto[0], ponto[1]);
    }else if(raio * raio == distancia){

        printf("(%d,%d): na fronteira", ponto[0], ponto[1]);
    }else{
        printf("(%d,%d): no exterior", ponto[0], ponto[1]);

    }
    
    return 0;
}