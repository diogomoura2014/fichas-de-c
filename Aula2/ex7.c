#include <stdio.h>

int main(){
    int max, value, i = 0; 
       // printf("insira um valor  ou se quiser parar prima 0\n");
    while( value !=0){
        //printf("valor: ");
        scanf("%d", &value);
        if(i == 0){
            i++;
            max = value;
        } 
        max = (value > max && value != 0) ? value : max;
    }
    printf("O valor maximo introduzido é %d\n", max);
    return 0;
}