#include <stdio.h>

int main(){
    int a,b,c;
    //printf("De uma equação do tipo ax^2 + bx + c\nIndique os valores de a, b e c respetivamente: \n");
    //printf("a = ");
    scanf("%d", &a);
    //printf("b = ");
    scanf("%d", &b);
    //printf("c = ");
    scanf("%d", &c);

    int discriminant = (b * b) - (4 * a * c);
    if(discriminant < 0) {
        printf("\nSem raizes");
        return 0;
    }else if( discriminant == 0 ){
        printf("\nRaizes duplas");
        return 0;
    }else {
        printf("\nRaizes distintas");
        return 0;
    }

}