#include <stdio.h>
#include <string.h>
#include <assert.h>

#define MAXLENGTH 100
int abs(int  x){
  return x < 0 ? -x : x;
}

int findNext(char word[],int startX, int startY, int soupX, int soupY, char soup[soupX][soupY], int coords[]){
  int i = 1,length = strlen(word);
  int flag = 0, j = 1;
  if(soup[startX][startY] == word[length - 1]){
    j = length - 2;
    flag = 1;
  }
  
  while(word[j] == soup[startX + i][startY - i]){
    if(abs(i) == length - 1){
      coords[0] = startX + i;
      coords[1] = startY - i;
      return flag;
    }
    i++;
    if(flag) j--;
    else j++;
  }
  i = 1;
  if(flag){
    j = length - 2;
  }else{
    j = 1;
  }
  while(word[j] == soup[startX + i][startY]){
    if(abs(i) == length - 1){
      
      coords[0] = startX + i;
      coords[1] = startY ;

      return flag;
    }
    i++; 
    if(flag) j--;
    else j++;
  }
  i = 1;
  if(flag){
    j = length - 2;
  }else{
    j = 1;
  }

  while(word[j] == soup[startX + i][startY + i]){
    if(abs(i) == length - 1){
      coords[0] = startX + i;
      coords[1] = startY + i;

      return flag;
    } 
    i++; 
    if(flag) j--;
    else j++;
  }
  i = 1;
  if(flag){
    j = length - 2;
  }else{
    j = 1;
  }

  while(word[j] == soup[startX][startY + i]){
    
     if(abs(i) == length - 1){
      coords[0] = startX ;
      coords[1] = startY + i;

      return flag;
     }
     i++; 
    if(flag) j--;
    else j++;
  }
  return -1;
}

int main(){
  int x, y, numberWords, startX, startY, response;
  

  scanf("%d %d\n", &x, &y);
  if( x >= 4 && y >= 4 &&  x <= 20 && y <= 20){
    char soup[x][y];
    for( int i = 0; i < x;  i++){
      for( int j = 0; j < y; j++){
        soup[i][j] = getchar();
      }
      getchar();
    }
    scanf("%d", &numberWords);
    assert(numberWords>=1 || numberWords<=100);
    for(int l = 0; l < numberWords; l++){
      int res[2]= {-1,-1};
      char word[100];
      scanf("%s\n", word);
      int length = strlen(word); 
      for(int i = 0; i < x; i++){
        for(int j = 0; j < y; j++){
          if(word[0] == soup[i][j] || word[length - 1] == soup[i][j]){
            startX = i;
            startY = j;
            int coords[2]= {-1,-1};
            response = findNext(word, startX, startY,x,y,soup, coords);
            if(coords[0] != -1){
              res[0] = coords[0];
              res[1] = coords[1];
              j = y;
              i = x;
            }
          }
        }
         
      }

      if(response == 1){

        printf("%d %d %d %d\n", res[0] +1, res[1]+1, startX+1, startY+1);
      }else if (response == 0){

        printf("%d %d %d %d\n", startX+1, startY+1, res[0] +1, res[1]+1);
      }
    }
  }
  return 0;
}