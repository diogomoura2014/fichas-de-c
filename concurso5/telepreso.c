#include <stdio.h>

char decodeChar(int count, int value, char table[10][4]){
  return table[value][count - 1];
}

int main(){
  int value = -1, flag = 1, lastValue, count = 0;
  char valuesTable[10][4] = {
  { '+'}, 
  { '_' },
  { 'A', 'B', 'C'}, 
  { 'D', 'E', 'F'}, 
  { 'G', 'H', 'I'}, 
  { 'J', 'K', 'L'}, 
  { 'M', 'N', 'O'}, 
  { 'P', 'Q', 'R', 'S' }, 
  { 'T', 'U', 'V'}, 
  { 'W', 'X', 'Y', 'Z' },
  };

  while(value!= 99){
    scanf("%d",&value);
    if(flag == 0 && value == lastValue){
      lastValue = value;
      count = 2;
      flag = 1;
    }else if( flag == 1 && value == lastValue){
      count++;
    }else if (value != lastValue){
      if(lastValue >= 0 && lastValue <= 9) printf("%c", decodeChar(count, lastValue, valuesTable));
      count = 1;
      flag = 0; 
      lastValue = value;
    }
  }
  printf("\n");
}