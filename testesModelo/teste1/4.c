#include <stdio.h>

int main(){
  int n, pos1= 0, count, pos2= 0, x, first;
  scanf("%d", &n);
  for (int i = 0; i < n; i++)
  {
    scanf("%d", &first);
    pos1 = pos2 = 0;
    count = 2;
    x = 1;
    while (x != 0){
      scanf("%d", &x);
      if(x > first && x % 2 == 0 && pos1 == 0 && x !=0){
        pos1 = count;
      }else if( x > first && x % 2 == 0 && pos2 == 0 && x!= 0){
        pos2 = count;
      }
      count++;
    }
    if(pos1 != 0){
      printf("%d ", pos1);
    }else{
      printf("- ");
    }
    if(pos2 != 0){
      printf(" %d\n", pos2);
    }else{
      printf(" -\n");
    }
    x = 0;
  }
  
}