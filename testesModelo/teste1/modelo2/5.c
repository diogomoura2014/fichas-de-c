#include <stdio.h>
int main(){
  int x = 1, count1 = 0, count2 = 0, min1 = 0, min2 = 0;
  while(x !=0){
    scanf("%d", &x);
    if(x < 0 && x < min1){
      min2 = min1;
      min1 = x;
      count2 = count1;
      count1 = 0;
    }else if(x < 0 && x< min2 && x != min1){
      min2 = x;
      count2 = 0;
    }
    if(x == min1 && x != 0){
      count1++;
    }else if(x == min2 && x != 0){
      count2++;
    }
  } 
  if(count1 != 0 ){
    printf("valor %d apareceu %d vezes\n", min1, count1);
  }

  if(count2!= 0){
    printf("valor %d apareceu %d vezes\n", min2, count2);
  }
  if(count1 == 0 && count2 == 0){
    printf("Valores negativos inexistentes\n");
  }
}