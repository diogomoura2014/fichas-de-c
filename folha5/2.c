#include <stdio.h>
#include <string.h>
#define MAXVALUE 500
int abs(int x){
  return x > 0 ? x : -x;
}

void reverseArray(char arr[]){
  int length = strlen(arr), j = 0;
  char tmp; 

  for(int i = 1; i < ((length)/2 + 1); i++){
    printf("resultado = %s\n", arr);
    tmp = arr[j];
    arr[j++]= arr[length-i];
    arr[length -i] = tmp;
  }

}

int readline(char x[]){
  int i;
  for(i = 0; i < MAXVALUE && (x[i] = getchar()) != '\n'; i++);

  if ( i == MAXVALUE){
    while(getchar() != '\n' );
    return 1;
  }
  x[i] = '\0';
  return 0;
}

char toChar(int x){
  char value; 
  if(x >= 0 && x <= 9){
    value = x +'0';
  }else{
    value = x + 'A' - 10;
  }
  return value;
}
int toInt( char x){
  int value;
  if(x >= '0' && x <= '9'){
    value = x - '0';
  }else{
    value = x - 'A' + 10;
  }
}

void finishString(char res[], int i, int diff){
  for(int j = i + diff; j < MAXVALUE ; j++) res[j] = '\0';
}

void addCarry(char res[], int carry, int diff , int i){
  res[i++ + diff] = ((carry==1 )? '1': '\0');
}

void addMissing(char arr[], char res[], int diff, int carry, int i, int b){
  for(int j = diff; j > 0 ; j--){
        int number = toInt(arr[j-1]);
        number += carry;
        carry = 0;

        if(number >= b){
          carry = 1;
          number -= b;
        }
        res[i + abs(j - diff)]= toChar(number);
      }
      res[i++ + diff] = ((carry )? '1': '\0' ) ;
     
      finishString(res, i, diff);
}

void soma(char x[], char y[], int b, char res[]){
  int lengthX = strlen(x), lengthY = strlen(y);
  int carry = 0, sum = 0, diff = abs(lengthX - lengthY);

  int length = lengthX <= lengthY ? lengthX : lengthY;
  char bigger = 'n';
  int i;
  if(lengthX > lengthY){
    bigger = 'X';
  }else if(lengthY > lengthX){
    bigger = 'Y';
  }

  if (b != 16){
    for( i = 0; i < length; i++ ){
        int number1 = toInt(x[lengthX - (i + 1)]);
        int number2 = toInt(y[lengthY - (i + 1)]);
        
        sum = number1 + number2 + carry; 
        carry = ((sum >= b) ? 1 : 0);
        sum -= ((carry) ? b : 0);
        res[i] = toChar(sum);
 
        // TODO arranjar 110 + 1231
        // TODO um carry =1 faz com que o resultado dê 1431
    }
    if(bigger == 'X'){
      addMissing(x, res, diff,carry, i, b);

    }else if(bigger == 'Y'){
      addMissing(y, res, diff, carry, i, b);

    }else {
      addCarry(res, carry, diff, i);
      finishString(res, i, diff);
    
    }
  }else{
    for( i = 0; i < length; i++ ){
      int  number1 = toInt(x[i]);
      int  number2 = toInt(y[i]);
       
      sum = number1 + number2 + carry; 
      printf("num1 = %d, num2 = %d\n", number1, number2);
      carry = ((sum >= b) ? 1 : 0);
      sum -= ((carry) ? b : 0);
      printf("sum = %d\n", sum);
      res[i] = toChar(sum);
      finishString(res, i, diff);
    }
  }
  if(strlen(res) > 1){
    reverseArray(res);
  }
}

int main(){ 
  int base;
  char number1[MAXVALUE] = "110", number2[MAXVALUE] = "1231", res[MAXVALUE+1];
  // scanf("%s", number1);
  // scanf("%s", number2);

  soma(number1, number2, 10, res);
  printf("resultado = %s\n", res);
}