#include <stdio.h>
#include <string.h>
#define MAXVALUE 300

#define TRUE 1
#define FALSE 0

int divisivel_11(char x[]){
  int len = strlen(x), sum1 = 0, sum2 = 0;
  for(int i = 0; i < len; i+= 2 ){
    int num = x[i] -'0';
    sum1 += num;
  }

  for(int i = 1; i < len; i+= 2 ){
    int num = x[i] -'0';
    sum2 += num;
  }
  return ( sum1 % 11 == sum2 % 11 ? TRUE : FALSE);
}

int divisivel_pot10(char x[], int p){
  int i = strlen(x), count = 0;

  while(i-- > 0 && count < p){
    if(x[i] == '0'){
      count++;
    }
  }
  return (count >= p ? 1 : 0 );
}
int par(int num){return ( num % 2 == 0 ? TRUE : FALSE );}

int divisivel_9(char x[]){
  int len = strlen(x), sum = 0;
  for(int i = 0; i < len; i++){
    int num = x[i] - '0';
    sum += num;
  }
  return ( sum % 9 == 0 ? TRUE : FALSE);
}

int readline(char x[]){
  int i;
  for(i = 0; i < MAXVALUE && (x[i] = getchar()) != '\n'; i++);

  if ( i == MAXVALUE){
    while(getchar() != '\n' );
    return 1;
  }
  x[i] = '\0';
  return 0;
}


int main(){
  char x[MAXVALUE] = "110000";
  int pot;
  scanf("%d", &pot);
  //readline(x);
  printf("%s divisivel por 10^%d = %d\n",x,  pot, divisivel_pot10(x,pot));
  printf("%s divisivel por 9 = %d\n",x, divisivel_9(x));
  printf("%s divisivel por 1 = %d\n",x, divisivel_11(x)); 
  return 0; 
}