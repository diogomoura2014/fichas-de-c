#include <stdio.h>
int potencia(int n, int pot){
    int res = 1;
    for (int i = 0; i < pot; i++)
    {
        res *= n ;
    }
return res;    
}

int main(){
    int n, k, s = 1;
    printf("Valor de k? ");
    scanf("%d", &k);
    printf("Valor de n? ");
    scanf("%d", &n);
    while(n != 0){
        s += potencia(k,n);
        n--;
    }
    printf("Soma = %d\n",s);
    return 0;
}