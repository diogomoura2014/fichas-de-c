#include <stdio.h>

#define NMAX  100
void imprimeLinha(int linhas, int col,int mat[][NMAX] ){
  for ( int i = 0; i < linhas; i++){
    for ( int j = 0; j < col; j++){
      printf("%d ", mat[i][j]);
    }
    printf("\n");
  }
}
int maximo(int mat[][NMAX], int linhas, int col){
  int max = 0;
  for(int i = 0; i < linhas; i++){
    for(int j = 0; j < col; j++){
      if( mat[i][j] > max){
        max = mat[i][j];
      }
    }
  }
  return max;
}

int main(){
  int col, linhas; 
  scanf("%d %d", &linhas, &col);
  int mat[linhas][NMAX];
  for (int i = 0; i < linhas; i++)
  {
    for (int j = 0; j < col; j++)
    {
      scanf("%d",&mat[i][j]); 
    }
  }

  imprimeLinha(linhas, col, mat); 
  printf("maximo = %d\n", maximo(mat, linhas, col));
  return 0;
}