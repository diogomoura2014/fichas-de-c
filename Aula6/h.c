#include <stdio.h>
int countValues(int *values,int length){
  int count = 0, value;
  for (int i = 1; i < length-1; i++)
  {
    value = values[i];
    if(value > 2*values[i - 1] && value> 2*values[i+1]) count++;
  }
  return count;
  
}

int main(){
  int n,count = 0, value, last, flag = 0;

  scanf("%d", &n);

    if(n >= 3 && n <= 1000){
      scanf("%d", &last);
      for(int i = 0; i< n-1; i++){
        scanf("%d", &value);

          if(flag && last>value*2){            
            count++;
            flag = 0;
          }else{

            if(last*2 < value){
              flag = 1;
            }
          }          
        last = value;
        
      }
      printf("%d\n", count);

  }
}