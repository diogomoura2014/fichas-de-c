#include <stdio.h>

int arrayAverage(int *arr, int length){
  int sum= 0, score, max= -1, min= 101, flagMax = 1, flagMin = 1;
  for(int i = 0; i <length; i++){  
    score= arr[i];
		if(score > 0 && score <= 100){
			sum += score;
		}
		if(score>=0 && score<= 100){
			if(score == max){
				flagMax = 0;
      }
			if(score == min){
				flagMin = 0;
			}
			if(score> max){
        		max = score;
				flagMax = 1;
      }	
			if(score<min){
				min = score;
				flagMin = 1;
      }
		}  
  }
	return (sum - (flagMin*min) - (flagMax*max))/(length - flagMax - flagMin);

}

int main(){
    int count; 
    scanf("%d", &count);
    int score[count];
    if(count >2 && count<= 10){
        for (int i = 0; i < count; i++){
            scanf("%d", &score[i]);
        }
				printf("%d\n", arrayAverage(score, count));
    }
}