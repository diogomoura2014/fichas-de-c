#include <stdio.h>
int abs(int n){
  return n < 0 ? -n : n;
}
int main(){
  int diff, height, width,r = 0,g = 0,b = 0, redflag = 1, greenflag = 0, blueflag = 0;
  scanf("%d %d", &height, &width);
  diff = abs(height - width);
  for(int i = 1; i <= height * 2; i+=2){
    if(redflag == 1){
      r +=i;
      redflag = 0;
      greenflag = 1;
    }else if(greenflag == 1){
      g +=i;
      greenflag = 0;
      blueflag = 1;
    }else if(blueflag == 1){
      b +=i;
      blueflag = 0; 
      redflag = 1;
    }
    printf("red %d green %d blue %d\n", redflag, greenflag, blueflag);
  }

  if(diff != 0){
    for(int i = 0; i < diff; i++){
      if(redflag){
        r +=height;
        redflag = 0;
        greenflag = 1;
      }else if(greenflag){
        g +=height;
        greenflag = 0;
        blueflag = 1;
      }else if(blueflag){
        b +=height;
        blueflag = 0; 
        redflag = 1;
      } 
    }
  }
  printf("%d %d %d\n", r,g,b);
}
