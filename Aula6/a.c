#include <stdio.h>
int abs(int n){
  return n< 0? -n: n;
}
int main(){
  int me, them, nfixtures, diff, lastpoints;
  scanf("%d %d %d", &me, &them, &nfixtures);
  lastpoints = nfixtures * 3;
  diff = me - them;
  if(diff >= lastpoints){
    printf(":-D\n");
  }else if(diff < lastpoints && diff > 0){
    printf(":-)\n");
  }else if(diff< 0 && abs(diff) < lastpoints ){
    printf(":-|\n");
  }else if(diff < 0 && abs(diff) >= lastpoints){
    printf(":-(\n");
  }
}