#include <stdio.h>
#include <math.h>

long int fatorial(int n){
    unsigned long long int sum = 1;
    while( n > 0 )
        sum *= n--;
    return sum;
    
}
double seno(double x, int k){
    double sum = 0.0;
    for (int i = 0; i <= k; i++)
    {
        sum += (pow(-1,i)/fatorial(2*i))*pow(x,2*i); 
        
    }
    return sum;
}

double cosseno(double x, int k){
    double sum = 0.0;
    for (int i = 0; i <= k; i++)
    {
        sum += (pow(-1,i)/fatorial(2*i +1))*pow(x,2*i+1); 
    }
    return sum;
}

int main(){
    long double x;
    long int k;
    scanf("%Lf %ld", &x,&k);
    printf("cos(x) = %f\nsen(x) = %f\n", cosseno(x, k), seno(x, k));
}