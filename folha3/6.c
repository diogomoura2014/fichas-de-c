#include <stdio.h>

  
int somadivprops(int n){
    int d = 2, s = 1, sum = 1;

    if(n > 1){
        if(n % 2 != 0){
            s = 2;
            d = 3;
            sum = 0;
        }
        while(d<= n/2){
            if(n%d== 0) sum += d; 
            d += s;
        }
    }
    return sum;

}
int perfeito(int n){
    if(somadivprops(n) == n){
        return 1;
    }
		return 0;
}
void imprimeperfeitos(int a, int b){
		for(int i = a; i <= b; i++){
			if(perfeito(i)){
				printf("%d\n", i);
			}
		}
}
int main(){
   long int a,b;
    scanf("%ld %ld", &a, &b);
    imprimeperfeitos(a,b);
    return 0;
}