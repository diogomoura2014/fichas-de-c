#include <stdio.h>
#include <math.h>

double leibniz(int n ){
    double sum = 0;
    for (int i = 0; i < n; i++)
    {
        sum += pow(-1, i)/(2*i + 1);
    }
    return 4 * sum;
}

int main(){
    int n;
    scanf("%d", &n);
    printf("%f\n",leibniz(n));
}