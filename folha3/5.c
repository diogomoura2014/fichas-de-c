#include <stdio.h>

void divisores(int n){
    int d = 2, s = 1;

    if(n != 1){
        printf("1 %d\n", n);
        if(n % 2 != 0){
            s = 2;
            d = 3;
        }
        while(d*d<= n){
            if(n%d== 0 && d*d!= n){
                printf("%d %d\n", d, n/d);
            }else if(d*d==n){
                printf("%d\n", d);
            }
            d += s;
        }
    }
}

int main(){
    int n;
    scanf("%d", &n);
    divisores(n);
}