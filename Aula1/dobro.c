#include <stdio.h>

int main(){

    int valor;
    
    printf("Indique um inteiro por favor -> \n");
    scanf("%d", &valor);

    printf("O dobro de %d = %d\n",valor, 2 * valor);

    return 0;
    
} 